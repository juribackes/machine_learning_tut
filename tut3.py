import numpy as np
import matplotlib.pyplot as plt

from sklearn.datasets import fetch_olivetti_faces
from sklearn.utils.validation import check_random_state

from sklearn.ensemble import ExtraTreesRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import RidgeCV


# scikit learn tutorial -  face  using supervised learning technique and different estimators
# https://scikit-learn.org/stable/auto_examples/plot_multioutput_face_completion.html#sphx-glr-auto-examples-plot-
# multioutput-face-completion-py

# loading the data set containing 400 [64*64] gray scale images of different faces
# one picture consists of 4096 pixels and is held in a 1D vector
data, targets = fetch_olivetti_faces(return_X_y=True)

# cutting the data into training and testing data (350 training examples)
# as expected increasing the training data achieves better results
train = data[targets < 35]
test = data[targets >= 35]

# number of faces we want to test
n_faces = 5
# pseudo random number generator (reproducible, returns the same numbers every time)
rng = check_random_state(4)
# returns an array of n_faces "random" numbers
face_ids = rng.randint(test.shape[0], size=(n_faces, ))
# test data set now consists of the n_faces random pictures
test = test[face_ids, :]

# number of pixels in one picture (4096)
n_pixels = data.shape[1]

predict = "upper_half" # default "lower_half"
# cutting the pictures in half (now each picture consists of 2048 pixels)
# upper half (input)
if predict is "upper_half":
    X_train = train[:, n_pixels // 2:]
    y_train = train[:, :(n_pixels + 1) // 2]

    X_test = test[:, n_pixels // 2:]
    y_test = test[:, :(n_pixels + 1) // 2]
elif predict is "lower_half":
    # lower half (output)
    X_train = train[:, :(n_pixels + 1) // 2]
    y_train = train[:, n_pixels // 2:]

    # doing the same for the test data
    X_test = test[:, :(n_pixels + 1) // 2]
    y_test = test[:, n_pixels // 2:]

# Fit estimators
ESTIMATORS = {
    "Extra trees": ExtraTreesRegressor(n_estimators=10, max_features=32,
                                       random_state=1),
    "K-nn": KNeighborsRegressor(),
    "Linear regression": LinearRegression(),
    "Ridge": RidgeCV(),
}

y_test_predict = dict()
for name, estimator in ESTIMATORS.items():
    # letting each estimator "learn" from the trainging data
    estimator.fit(X_train, y_train)
    # the estimator predicts the lower half of the face of the test pictures
    y_test_predict[name] = estimator.predict(X_test)

# plotting the completed faces
# 64x64 image
image_shape = (64, 64)

n_cols = len(ESTIMATORS) + 1

plt.figure(figsize=(2. * n_cols, 2.26 * n_faces))
plt.suptitle("Face completion with multi-output estimators", size=16)

for i in range(n_faces):
    if predict is "upper_half":
        true_face = np.hstack((y_test[i], X_test[i]))
    elif predict is "lower_half":
        # stack the lower and upper half of the face together
        true_face = np.hstack((X_test[i], y_test[i]))

    # if i is not 0
    if i:
        sub = plt.subplot(n_faces, n_cols, i * n_cols + 1)
    # if i is 0
    else:
        sub = plt.subplot(n_faces, n_cols, i * n_cols + 1,
                          title="true faces")

    sub.axis("off")
    # reshaping the 1D vector to a 64x64 image
    sub.imshow(true_face.reshape(image_shape),
               cmap=plt.cm.gray,
               interpolation="nearest")

    for j, est in enumerate(sorted(ESTIMATORS)):
        # stack the lower and upper half of the face together
        if predict is "upper_half":
            completed_face = np.hstack((y_test_predict[est][i], X_test[i]))
        elif predict is "lower_half":
             # stack the lower and upper half of the face together
            completed_face = np.hstack((X_test[i], y_test_predict[est][i]))

        # if i is not 0
        if i:
            sub = plt.subplot(n_faces, n_cols, i * n_cols + 2 + j)
        # if i is 0
        else:
            sub = plt.subplot(n_faces, n_cols, i * n_cols + 2 + j,
                              title=est)

        sub.axis("off")
        # reshaping the 1D vector to a 64x64 image
        sub.imshow(completed_face.reshape(image_shape),
                   cmap=plt.cm.gray,
                   interpolation="nearest")

plt.show()