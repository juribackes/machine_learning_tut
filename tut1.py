import numpy as np
import matplotlib.pyplot as plt

from sklearn import datasets
from sklearn import svm
from sklearn.model_selection import train_test_split


# scikit learn tutorial -  digit recognition
# following https://www.youtube.com/watch?v=KTeVOb8gaD4&list=PLQVvvaa0QuDd0flgGphKCej-9jp-QdzZ3

# loading the dataset
digits = datasets.load_digits()
# classifier - right know this is a blackbox
# gamma is the 'leap size' of the gradient descend
# big gamma - big leap
clf = svm.SVC(gamma=0.001, C=100)

# reshaping ('flatening') the data
n_samples = len(digits.images)
data = digits.images.reshape((n_samples, -1))

# Split data into train and test subsets
x, y = digits.data[:-1], digits.target[:-1]
x_test, y_test = digits.data[-1:], digits.target[-1:]
x_test.reshape(1, -1)
# the algorithm 'learns' from the 'true' examples
clf.fit(x, y)

# do prediction and show the image of the test element
print('Prediction:', clf.predict(x_test))
plt.imshow(digits.images[-1], cmap=plt.cm.gray_r, interpolation="nearest")
plt.show()

