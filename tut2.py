import pandas as pd
import numpy as np
from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_boston

# scikit learn tutorial -  housing price
# predicting the housing price in Boston considering the following 13 attributes
# 'CRIM', 'ZN', 'INDUS', 'CHAS', 'NOX', 'RM', 'AGE', 'DIS', 'RAD', 'TAX', 'PTRATIO', 'B', 'LSTAT']

# Loading the dataset
boston = load_boston()

# Loading dataframes
df_x = pd.DataFrame(boston.data, columns=boston.feature_names)
df_y = pd.DataFrame(boston.target)

# Regression model
reg = linear_model.LinearRegression()

# Split data into train and test subsets
x_train, x_test, y_train, y_test = train_test_split(df_x, df_y, test_size=0.005)

# The algorithm 'learns' from the 'true' examples
reg.fit(x_train, y_train)

# predict the housing price in 1000$
predictions = reg.predict(x_test)

# print the prediction
i = 0
for val in y_test[0]:
    print("Predict:", predictions[i], "True:", val)
    i += 1

print("coefficients:", reg.coef_) # Estimated coefficients for the linear regression problem
print("intercept:", reg.intercept_) # Independent term in the linear model



